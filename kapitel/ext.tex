%a là Eisenbud Seite 449ff, Kapitel 18.1.1
\section{Ext und Tiefe}

$\Ext_R^*(\_,\_)$ ist ein $R$-bilinearer Bifunktor der homologischen Algebra.
Eine Einführung in die zugehörige Theorie findet man in \autocite{rotman} oder \autocite[Appendix A.3]{eis}.

In diesem Abschnitt wird bewiesen, dass sich durch die Formel
\[\depth(I,N)=\min\{r\in \NNN\mid\Ext^{r}_{R}(R/I,N)\neq0\}\]
die Tiefe eines Ideals mittels Ext berechnen lässt.
Wir beweisen sogar eine allgemeinere Version des Satzes für $R$-Moduln.
Vorher beweisen wir noch ein Hilfslemma:

%im Beweis von 18.4
\lemma[\autocite{eis} im Beweis von 18.4]{Seien $R$ ein Ring und $M$ und $N$ zwei endlich erzeugte $R$-Moduln.
Es gilt $\ann(M)N=N$ genau dann, wenn $\ann M+\ann N=R$.}\label{eis:18.4Hilfslemma}

\proof{Gelte $\ann(M)N=N$.
Nach \autocite[Korollar 4.7]{eis} existiert ein $a\in\ann M$ mit $(1-a)N=0$.
Also ist $1-a\in\ann N$ und damit $1\in\ann M+\ann N$.

Gelte nun $\ann M+\ann N=R$.
Da $N$ ein $R$-Modul ist, folgt $\ann(M)N\subseteq N$.
Seien $a\in\ann M$ und $b\in\ann N$ mit $a+b=1$ und $n\in N$.
Dann ist
\[n=1\cdot n=(a+b)n=an+bn=an+0=an\in\ann(M)N\]
und damit gilt auch $N\subseteq\ann(M)N$. Folglich ist $\ann(M)N=N$. \qed}

%18.4
\prop[\autocite{eis} 18.4]{Seien $R$ ein Ring und $M$ und $N$ zwei endlich erzeugte $R$-Moduln.
Falls $\ann M+\ann N=R$, dann ist $\Ext^r_R(M,N)=0$ für alle $r\in\NNN$.
Ansonsten gilt für $\ann M+\ann N\neq R$:
\[\depth(\ann(M),N)=\min\{r\in \NNN\mid\Ext^{r}_{R}(M,N)\neq0\}\]}\label{eis:18.4}

\proof{Gelte $\ann M+\ann N=R$, dann existieren $a\in\ann M$ und $b\in\ann N$ mit $a+b=1$.
Da $\Ext$ ein $R$-bilinearer Bifunktor ist, gilt für alle $r\in\NNN$:
\begin{align*}
&\Ext^r_R(M,N)=1\cdot\Ext^r_R(M,N)=(a+b)\cdot\Ext^r_R(M,N)=a\cdot\Ext^r_R(M,N)+b\cdot\Ext^r_R(M,N)=\\
=&\Ext^r_R(aM,N)+\Ext^r_R(M,bN)=\Ext^r_R(0,N)+\Ext^r_R(M,0)=0+0=0
\end{align*}

Gelte nun $\ann M+\ann N\neq R$.
Damit gilt nach \ref{eis:18.4Hilfslemma} $\ann(M)N\neq N$.
Somit ist $d\define\depth(\ann M,N)<\infty$.
Weiterhin sind $M\neq0$ und $N\neq0$.

Sei $A_d$ die Aussage, die besagt, dass für $\depth(\ann M,N)\leq d$ gilt:
\[\depth(\ann M,N)=\min\{r\in \NNN\mid\Ext^{r}_{R}(M,N)\neq0\}\]
Wir beweisen die Aussage per Induktion über $d$.

Sei $d=0$, dann ist Folgendes zu zeigen:
\[\Ext^0_R(M,N)=\Hom_R(M,N)\neq 0.\]
Da $\depth(\ann M,N)=0$, besteht $\ann M$ aus Nullteilern von $N$.
Demnach ist nach \autocite[Korollar 3.2]{eis} $\ann M$ in einem assoziierten Primideal $P$ von $N$ enthalten.
Falls wir zeigen können, dass $\Hom_R(M,N)_P\neq0$, dann ist auch $\Hom_R(M,N)\neq0$.

Betrachten wir deswegen $\Hom_R(M,N)_P$.
Es gilt $\Hom_R(M,N)_P=\Hom_{R_P}(M_P,N_P)$ nach \autocite[Proposition 2.10]{eis}, da $M$ endlich erzeugt ist.
Daher ist oBdA nach Umbennenung $(R,P)$ ein lokaler Ring, dann enthält $N$ eine Kopie des Restklassenkörpers $R/P$.
Da $M\neq0$ ist nach \ref{eis:4.8} $M/PM\neq0$, somit ist $M/PM$ eine direkte Summe von Kopien von $R/P$.
Folglich gibt es einen Modulhomomorphismus $M\to R/P\subseteq N$ ungleich $0$, damit ist $\Hom(M,N)\neq0$.

Sei nun $d\geq1$, gelte $A_{d-1}$ und sei $x\in\ann M$ ein Nichtnullteiler von $N$.
Es gilt
\[\ann(M)(N/xN)\neq N/xN\]
und nach \ref{cmrs:1.2.10d} $\depth(\ann(M),N/xN)=d-1$.
Nach Induktionsvoraussetzung ist
\[\Ext^i_R(M,N/xN)\neq0\]
für $i=d-1$ und $\Ext^i_R(M,N/xN)=0$ für $0\leq i<d-1$.
Wenn wir auf die kurze exakte Sequenz
\[0\to N \stackrel{x}{\to} N\to N/xN\to 0\]
den Funktor $\Ext_R(M,\_)$ anwenden, erhalten wir nach \autocite[Korollar 6.46]{rotman} die lange exakte Sequenz
\begin{align*}
0 &\to \Ext_R^0(M,N) \stackrel{\phi_0}{\to} \Ext_R^0(M,N) \to \Ext_R^0(M,N/xN) \to \\
&\to \Ext_R^1(M,N) \stackrel{\phi_1}{\to} \Ext_R^1(M,N) \to \Ext_R^1(M,N/xN) \to \ldots \\
\ldots &\to \Ext_R^j(M,N) \stackrel{\phi_j}{\to} \Ext_R^j(M,N) \to \Ext_R^j(M,N/xN) \to \ldots
\end{align*}
Da $x\in\ann M$ und $\Ext$ ein $R$-bilinearer Bifunktor ist, ist das Bild der Abbildungen
\[\im(\phi_j)=\Ext_R^j(M,xN)=\Ext_R^j(xM,N)=\Ext_R^j(0,N)=0.\]
Es sind also alle $\phi_j$ Nullabbildungen.
Deswegen lässt sich die lange exakte Sequenz in kurze exakte Sequenzen der Form
\[0 \to \Ext_R^{j-1}(M,N) \to \Ext_R^{j-1}(M,N/xN) \to \Ext_R^{j}(M,N) \to 0\]
für $j\in \NNN$ und $\Ext_R^{-1}(M,N)=\Ext_R^{-1}(M,N/xN)\define0$ aufteilen.
Nach Induktionsvoraussetzung ist $\Ext^i_R(M,N/xN)=0$ für $0\leq i<d-1$.
Damit haben die kurzen exakten Sequenzen die Form
\[0 \to \Ext_R^{j-1}(M,N) \to 0 \to \Ext_R^{j}(M,N) \to 0\]
für $0\leq j<d$.
Daraus folgt $\Ext^j_R(M,N)=0$ für $0\leq j<d$.

Betrachten wir nun die kurze exakte Sequenz für $j=d$:
\[0 \to 0 \to \Ext_R^{d-1}(M,N/xN) \to \Ext_R^{d}(M,N) \to 0\]
Nach Induktionsvoraussetzung ist $\Ext^{d-1}_R(M,N/xN)\neq0$.
Da die Sequenz exakt ist, ist $\Ext^{d-1}_R(M,N/xN)\cong\Ext_R^{d}(M,N)\neq0$.
Damit haben wir die Aussage bewiesen. \qed}

\kor{Seien $R$ ein Ring, $N$ ein $R$-Modul und $I\subseteq R$ ein Ideal.
Dann gilt:
\[\depth(I,N)=\min\{r\in \NNN|\Ext^{r}_{R}(R/I,N)\neq0\}\]
}

\proof{Es gilt $I=\ann R/I$.
Nach Anwendung von \ref{eis:18.4} folgt die Aussage.
\qed}

Da man $\Ext$ mithilfe eines Computeralgebrasystems berechnen kann, lässt sich mit der obigen Formel auch die Tiefe berechnen:
Man überprüft iterativ, ob $\Ext^{r}_{R}(M,N)\neq 0$.
Details zur Berechnung von $\Ext$ mit dem Computeralgebrasystem Macaulay 2 findet man in \autocite{macaulay2} und mit dem Computeralgebrasystem SINGULAR in \autocite{greuelpfister}.

Aus der Beschreibung der Tiefe mit $\Ext$ lassen sich einige Ungleichungen ableiten, die eine Berechnung der Tiefe für kurze exakte Sequenzen vereinfachen.

%Korollar 18.6, Proposition 1.2.9 cmrs
\kor[\autocite{eis} 18.6, \autocite{cmrs} 1.2.9]{Seien $R$ ein Ring, $I\subseteq R$ ein Ideal und $0 \to U \to M \to N \to 0$ ein kurze exakte Sequenz endlich erzeugter $R$-Moduln.
Dann gilt:
\begin{enumerate}
\item $\depth(I,M) \geq \min \{ \depth(I,U),\depth(I,N) \}$
\item $\depth(I,U) \geq \min \{ \depth(I,M),\depth(I,N)+1 \}$
\item $\depth(I,N) \geq \min \{ \depth(I,U)-1,\depth(I,M) \}$
\end{enumerate}
}\label{eis:18.6}

\proof{Wenn wir auf die kurze exakte Sequenz $0\to U \to M \to N \to 0$ den Funktor $\Ext_R(R/I,\_)$ anwenden, erhalten wir nach \autocite[Korollar 6.46]{rotman} die lange exakte Sequenz
\begin{align*}
\cdots \to &\Ext_R^{i-1}(R/I,U) \to \Ext_R^{i-1}(R/I,M) \to \Ext_R^{i-1}(R/I,N) \to\\
\to &\Ext_R^{i}(R/I,U) \to \Ext_R^{i}(R/I,M) \to \Ext_R^{i}(R/I,N) \to\\
\to &\Ext_R^{i+1}(R/I,U) \to \Ext_R^{i+1}(R/I,M) \to \Ext_R^{i+1}(R/I,N) \to \cdots
\end{align*}

Sei $i\define \min \{ \depth(I,U),\depth(I,N) \}$, dann hat nach \ref{eis:18.4} für $j<i$ die Sequenz die Form
\[\cdots \to \Ext_R^{j}(R/I,U)=0 \to \Ext_R^{j}(R/I,M) \to \Ext_R^{j}(R/I,N)=0 \to \cdots\]
Damit ist $\Ext_R^{j}(R/I,M)=0$ für $j<i$ und deswegen nach \ref{eis:18.4} $\depth(I,M)\geq i$.

Die beiden anderen Ungleichungen lassen sich analog zeigen.
\qed}
