%a là Eisenbud Seite 225f, Kapitel 9
\chapter{Kodimension und Tiefe}
In diesem Kapitel werden die Begriffe der Kodimension und Tiefe eines Ideals eingeführt und einige Eigenschaften dieser bewiesen.
Insbesondere wird gezeigt, dass die Tiefe immer kleiner gleich der Kodimension ist und sich die Tiefe mithilfe des Verschwindungsverhaltens des Ext-Bifunktors berechnen lässt.

Alle in dieser Arbeit betrachteten Ringe sind kommutativ mit einem Eins-Element und noethersch.
$\NNN$ bezeichnet die natürlichen Zahlen mit $0$, $\NN$ die natürlichen Zahlen ohne~$0$.

\defn{Ein Ring $R$ ist \textbf{noethersch}, falls jedes Ideal von $R$ endlich erzeugt ist.}

\lemma[\autocite{eis} Abschnitt 1.4]{Ein Ring $R$ ist genau dann noethersch, wenn er die \textbf{aufsteigende Kettenbedingung} erfüllt:
Jede aufsteigende Kette von Idealen $P_0\subseteq P_1 \subseteq P_2 \subseteq \ldots$ in $R$ wird stationär, das heißt es existiert ein $n\in\NNN$, sodass für alle $i\geq n$ gilt:
$P_i=P_{i+1}$.}

\proof{Seien $R$ noethersch und $P_0\subseteq P_1 \subseteq P_2 \subseteq \ldots$ eine aufsteigende Kette von Idealen von $R$.
Dann ist auch $\bigcup_{i\in\NNN}P_i$ ein Ideal von $R$ und damit endlich erzeugt.
Dann muss es ein $n\in\NNN$ geben, so dass alle Erzeuger in $P_n$ enthalten sind und damit gilt für alle $i\geq n$:
$P_i=P_{i+1}$.

Seien nun $R$ ein Ring, für den die aufsteigende Kettenbedingung gilt und $I$ ein Ideal.
Man kann durch geschickte Wahl von Elementen aus $I$ eine aufsteigende Idealkette
\[(f_1) \subsetneq (f_1,f_2) \subsetneq (f_1,f_2,f_3) \subsetneq \cdots\]
erhalten, indem man sukzessive ein Element $f_{i+1}$ aus dem mengentheoretischen Komplement $I\setminus(f_1,\ldots,f_i)$ wählt.
Dies ist solange möglich, bis für ein $n\in\NNN$ gilt:
$I=(f_1,\ldots,f_n)$.
Da $R$ die aufsteigende Kettenbedingung erfüllt und damit die Idealkette stationär wird, existiert so ein $n$.
Also ist $I$ endlich erzeugt und $R$ noethersch. \qed}

In dieser Arbeit wird häufig mit Lokalisierungen gearbeitet, eine Einführung befindet sich in \autocite[Kapitel 2]{eis}.

\defn{Seien $R$ ein Ring, $M$ ein $R$-Modul und $P\subsetneq R$ ein Primideal.
Dann ist die Lokalisierung von $M$ bei $P$ definiert als $M_P=M[(R\setminus P)^{-1}]$.}

% In dieser Arbeit wird regelmäßig mit Lokalisierungen von Ringen gearbeitet, im Anhang \ref{Lokalisierung} findet sich eine kurze Wiederholung der grundlegenden Begriffe.

\defn{Ein Paar $(R,\m)$ heißt \textbf{lokaler Ring}, falls $R$ ein Ring und $\m$ das einzige maximale Ideal von $R$ ist.
Der Körper $R/\m$ wird Restklassenkörper genannt.}

\section{Kodimension}

In diesem Abschnitt werden die in dieser Arbeit verwendeten Begriffe der Dimensionstheorie, insbesondere die Kodimension eines Ideals, eingeführt.
Eine ausführliche Einführung findet man z.B. in \autocite{eis}, \autocite{matsumura} oder \autocite{atiyahmacdonald}.

\defn{Die \textbf{Krull-Dimension} oder auch \textbf{Dimension} eines Ringes~$R$, geschrieben $\boldsymbol{\dim R}$, ist das Supremum der Längen aller Ketten von Primidealen in~$R$, in Formelsprache:
\[\dim R \define \sup \{n\in\NNN \mid \exists P_n \supsetneq P_{n-1} \supsetneq \cdots \supsetneq P_0 \text{ Primidealkette in } R\}\]
}\label{defn:DimensionRing}

Eine Kette von Primidealen $P_n \supsetneq P_{n-1} \supsetneq \cdots \supsetneq P_0$ hat dabei die Länge~$n$.

\defn{Sei $I \subsetneq R$ ein Ideal. Die $\boldsymbol{\textbf{Dimension von }I}$ eines Ringes~$R$, geschrieben $\boldsymbol{\dim I}$, ist die Dimension des Ringes $R/I$.}

\defn{Sei~$I$ ein Primideal von~$R$. Die $\boldsymbol{\textbf{Kodimension von }I}$, geschrieben \\ $\boldsymbol{\codim I}$, ist die Dimension der Lokalisierung~$R_I$.
Für ein beliebiges Ideal $I\subsetneq R$ ist $\codim I$ definiert als das Minimum der Kodimensionen der Primideale, die~$I$ enthalten, in Formelsprache:
\[\codim I \define \min\{\codim P \mid P \supseteq I, P \subseteq R \text{ Primideal}\}\]
}\label{defn:KodimensionIdeal}

Die Kodimension eines Ideals wird bei anderen Autoren auch Höhe des Ideals (vgl. \autocite{atiyahmacdonald,matsumura,cmrs,ag}) genannt.


\lemma[\autocite{eis} Kommentar nach Definition $\codim$]{Für ein Primideal~$I$ von~$R$ gilt: $\codim I$ ist das Supremum über die Länge aller absteigenden Ketten von Primidealen, die bei~$I$ anfangen, in Formelsprache:
\[\codim I = \sup\{n\in\NNN \mid I \supsetneq P_{n-1} \supsetneq \cdots \supsetneq P_0 \text{ Primidealkette in } R \text{ der Länge } n\}\]
}\label{lem:kodimKette}

\proof{Sei $\phi:R\to R_I, r\mapsto \frac{r}{1}$ die natürliche Projektionsabbildung.
Dann ist $P \mapsto \phi^{-1}(P)$ nach \autocite[Proposition 2.2]{eis} eine inklusionserhaltende Bijektion zwischen den Primidealen von $R_I$ und den Primidealen von $R$, die in $I$ enthalten sind.
Deswegen ist
\[\left( P_n \supsetneq P_{n-1} \supsetneq \cdots \supsetneq P_0 \right) \mapsto \left(\phi^{-1}(P_n) \supsetneq \phi^{-1}(P_{n-1}) \supsetneq \cdots \supsetneq \phi^{-1}(P_0)\right)\]
eine Bijektion zwischen den Primidealketten von $R_I$ und den Primidealketten von $R$, für die $\phi^{-1}(P_n)\subseteq I$ gilt.
Insbesondere folgt die zu zeigende Aussage.
\qed}

\defn{Der Annihilator eines $R$-Moduls~$M$ ist definiert als
\[\ann M = \{r \in R \mid rM = 0\}.\]}

\lemma{$\ann M$ ist ein Ideal von $R$.}

\proof{Seien $m\in M$, $a,b\in \ann M$ und $r\in R$.
Es ist $0\cdot m=0$ und damit $0\in\ann M$.
Außerdem ist $(a+b)m=am+bm=0+0=0$.
Also gilt $a+b\in\ann M$.
Weiterhin ist $(ra)m=r(am)=r\cdot 0=0$.
Daher ist $ra\in\ann M$.
\qed}

\defn{Für einen $R$-Modul~$M$ sind die \textbf{Dimension} und \textbf{Kodimension} definiert als die Dimension und Kodimension des Annihilators von~$M$.}

Für ein Ideal $I$ haben wir damit zwei in Konflikt stehende Dimensionsbegriffe, da man $I$ auch als $R$-Modul auffassen kann.
In dieser Arbeit wird die Konvention aus \autocite{eis} übernommen, dass immer die (Ko)dimension als Ideal gemeint ist.

Wie der Name (Ko)dimension vermuten lässt, handelt es sich hierbei um eine geometrisch motivierte Definition.
Eine ausführliche Diskussion dieser Andeutung befindet sich in \autocite{ag}.

Ein wichtiges Theorem der Dimensionstheorie ist der Hauptidealsatz.
Wir werden diesen in dieser Arbeit häufig benutzen, aber nicht beweisen.

\thm[Hauptidealsatz \autocite{eis} 10.2]{Seien $x_1,\ldots,x_n\in R$ und $P\subseteq R$ ein Primideal, welches minimal unter den Primidealen ist, die $x_1,\ldots,x_n$ enthalten.
Dann gilt:
$\codim P\leq n$.
}\label{eis:10.2}

\proof{Siehe \autocite[Theorem 10.2]{eis} oder \autocite[Korollar 11.16]{atiyahmacdonald}.
\qed}

Das folgende Lemma wird erst im Beweis von \ref{cmrs:2.1.6} benötigt, ist aber aus thematischen Gründen schon hier aufgeführt.

%cmrs A.2
\lemma[\autocite{cmrs} A.2]{Seien $R$ ein Ring und $I\subsetneq R$ ein Ideal mit $\codim I=n$.
Dann existieren $x_1,\ldots,x_n\in I$, so dass für alle $i=0,\ldots,n$ gilt:
$\codim(x_1,\ldots,x_i)=i$.
}\label{cmrs:A.2}

\proof{Wähle sukzessive Elemente $x_i$, die nicht in den minimalen Primidealen von $(x_1,\ldots,x_{i-1})$ liegen.
Solch eine Wahl ist nach \ref{eis:3.3} möglich.
\qed}

Das folgende Lemma ist für die explizite Berechnung der Dimension eines lokalen Rings praktisch.

%10.7
\lemma[\autocite{eis} 10.7]{Sei $(R,\m)$ ein lokaler Ring.
Sei $d\in\NNN$ die kleinste Zahl, so dass Elemente $x_1,\ldots,x_d\in\m$ und ein $n\in\NNN$ existieren mit $\m^n\subseteq(x_1,\ldots,x_d)$.
Dann ist $\dim R=d$.
}\label{eis:10.7}

\proof{Siehe \autocite[Korollar 10.7]{eis}.
\qed}

\defn{Seien $R$ ein lokaler Ring und $x_1,\ldots,x_d$ wie in \ref{eis:10.7}.
Dann wird $x_1,\ldots,x_d$ \textbf{Parametersystem} von $R$ genannt.
}
