%a là Eisenbud Seite 419ff, Kapitel 17
\section{Reguläre Folgen und Tiefe}

\defn{Sei~$R$ ein Ring und~$M$ ein $R$-Modul. Eine Folge $x_1,\ldots,x_n$ mit $x_i \in R$ heißt $\boldsymbol{M \textbf{-reguläre Folge}}$ oder $\boldsymbol{M\textbf{-Folge}}$, falls gilt:
\begin{enumerate}
\item $(x_1,\ldots,x_n)M \neq M$, und
\item Für $i=1,\ldots,n$ ist $x_i$ ein Nichtnullteiler von $M/(x_1,\ldots,x_{i-1})M$.
\end{enumerate}}

Das Prototypbeispiel einer regulären Folge ist die Folge der Variablen in einem Polynomring:

\bsp{Wir betrachten den Ring $k[x_1,\ldots,x_n]$ und die Folge
\[x_1,\ldots,x_n\]
Es gilt $(x_1,\ldots,x_n)\neq k[x_1,\ldots,x_n]$ und $x_i$ ist ein Nichtnullteiler in $k[x_1,\ldots,x_n]/(x_1,\ldots,x_{i-1})=k[x_i,\ldots,x_n]$.
Also ist $x_1,\ldots,x_n$ eine $k[x_1,\ldots,x_n]$-reguläre Folge.
}

Zwischen $M$-Folgen und der Kohomologie des Koszul-Komplexes besteht ein Zusammenhang, den wir uns im Folgenden zunutze machen wollen.
Eine kurze Zusammenfassung der Theorie befindet sich im Anhang \ref{Koszul-Komplex}, eine ausführliche Erklärung befindet sich in \autocite[Kapitel 17]{eis}.

In lokalen Ringen ist nach \autocite[Korollar 17.2]{eis} eine reguläre Folge in jeder Reihenfolge der Elemente wieder eine reguläre Folge, im Allgemeinen stimmt das jedoch nicht.
Betrachte dazu das folgende Beispiel:

%Bsp. 17.3
\bsp[\autocite{eis} 17.3]{Seien $k$ ein Körper und $R=k[x,y,z]/(x-1)z$.
Wir betrachten die Folge
\[x,(x-1)y\]
Das von beiden Elementen aufgespannte Ideal $(x,(x-1)y)=(x,y)\neq R$ ist nicht der ganze Ring, also ist die erste Eigenschaft einer regulären Folge erfüllt.
Es ist $x$ ein Nichtnullteiler in $R$ und $(x-1)y=-y$ ein Nichtnullteiler in $R/(x)=k[y,z]/(z)=k[y]$.
Damit ist $x,(x-1)y$ eine $R$-reguläre Folge.

Betrachten wir nun die Folge in der anderen Reihenfolge
\[(x-1)y,x\]
Es gilt $(x-1)y\cdot z=(x-1)zy=0y=0$ in $R=k[x,y,z]/(x-1)z$, also ist $(x-1)y,x$ keine $R$-reguläre Folge.
}

\defn{Sei~$I$ ein Ideal eines Ringes~$R$ und~$M$ ein endlich erzeugter $R$-Modul, für den gilt: $IM \neq M$.
Dann ist die $\boldsymbol{\textbf{Tiefe von }I\textbf{ in }M}$, geschrieben $\boldsymbol{\depth (I,M)}$, gleich der Länge einer (und damit aller, siehe \ref{eis:17.4}) maximalen $M$-regulären Folge in~$I$.
Für~$M=R$ sprechen wir einfach von der Tiefe von~$I$.
Gilt~$IM=M$, definieren wir $\depth(I,M)\define\infty$.}

Es folgen einige Beispiele.

\bsp{\mbox{}
\begin{enumerate}
\item Sei $I$ ein nilpotentes Ideal eines Ringes $R$, das heißt es existiert ein $n\in \NN$ mit $I^n=0$.
Jedes Element von $I$ ist ein Nullteiler, da für jedes Element $a\in I$ gilt: $0=a^n=a\cdot a^{n-1}$, also ist $\depth(I)=0$.
\item Sei $P$ ein zu einem $R$-Modul $M$ assoziiertes Primideal, also $P$ der Annihilator eines Elementes von $M$.
Dann ist wieder jedes Element von $P$ ein Nullteiler, damit gilt $\depth(P,M)=0$.
\item Sei $k[x]$ der Polynomring in einer Variablen über einen Körper $k$.
Wir wollen die Tiefe des maximalen Ideals $(x)$ ausrechnen.
$x$ ist eine $k[x]$-reguläre Folge, da $xk[x]\neq k[x]$ und $x$ ein Nichtnullteiler in $k[x]$ ist.

Da jedes Element aus $k\setminus\{ 0\}$ invertierbar ist, kann keines davon Teil einer regulären Folge sein.
Angenommen, es gibt eine reguläre Folge $ax^n,bx^m$ der Länge zwei mit $a,b\in k\setminus\{ 0\}$.
Da $k[x]$ ein lokaler Ring ist, können wir oBdA die beiden Elemente so sortieren, dass $n\geq m$.
Damit gilt $bx^m\cdot (ab^{-1}x^{n-m})=ax^n$.
Dann ist aber $bx^m$ ein Nullteiler von $k[x]/(ax^n)$, somit kann $ax^n,bx^m$ keine reguläre Folge gewesen sein.

%Nach \ref{eis:18.2} ist $1\leq\depth((x),k[x])\leq\codim((x))=1$. Daher gilt $\depth((x),k[x])=1$.
\end{enumerate}
}\label{bsp:tiefe1}

%S.67
\defn{Der \textbf{Träger} eines $R$-Moduls~$M$, geschrieben $\boldsymbol{\supp(M)}$, ist die Menge der Primideale~$P\subsetneq R$ mit~$M_P \neq 0$.}

%2.7
\lemma[\autocite{eis} 2.7]{Seien $M$ ein endlich erzeugter $R$-Modul und $P\subsetneq R$ ein Primideal.
Es gilt $P\in\supp M$ genau dann, wenn $\ann M\subseteq P$.
}\label{eis:2.7}

\proof{Nach \autocite[Proposition 2.1]{eis} gilt $M_P=0$ genau dann, wenn ein Element $x\in\ann M$ existiert mit $x\notin P$.
\qed}

%18.1
\lemma[\autocite{eis} 18.1]{Sei~$R$ ein Ring und~$P$ ein Primideal im Träger eines endlich erzeugten $R$-Moduls~$M$.
Dann lokalisiert jede $M$-Folge in~$P$ zu einer $M_P$-Folge.
Damit gilt für jedes Ideal~$I \subseteq P$: $\depth(I,M)\leq\depth(I_P,M_P)$ (bzgl. $R$ bzw. $R_P$).

Für jedes Ideal~$I$ existiert ein maximales Ideal~$P$ im Träger von~$M$ mit:
$\depth(I,M)=\depth(I_P,M_P)$.
Insbesondere gilt für maximale Ideale~$P$:
$\depth(P,M)=\depth(P_P,M_P)$.}\label{eis:18.1}

\proof{Seien~$R$ ein Ring, $M$ ein endlich erzeugter $R$-Modul und~$P\in\supp(M)$.
Sei $x_1,\ldots,x_n$ eine $M$-Folge.
Angenommen $P_PM_P = M_P$, dann wäre nach \ref{eis:4.8} $M_P=0$.
Das stellt einen Widerspruch dazu dar, dass $P \in \supp(M)$.
Also ist $P_PM_P\neq M_P$.
Bleibt zu zeigen, dass für $i=1,\ldots,n$ $x_i$ ein Nichtnullteiler von $M_P/(x_1,\ldots,x_{i-1})M_P$ ist.
Seien dafür $y=\frac{a}{b}\in M_P$ mit $x_i\cdot y=0$ und $a\in M$, $b\in(R\setminus P)$.
Multiplikation der Gleichung mit~$b$ liefert $x_i\cdot a=0$.
Da $x_i$ ein Nichtnullteiler von $M/(x_1,\ldots,x_{i-1})M$ ist, ist $a=0$ und damit $y=0$.
Folglich ist $x_1,\ldots,x_n$ eine $M_P$-Folge.
Aus der Definition der Tiefe ergibt sich daher für ein Ideal $I \subseteq P$:
$\depth(I,M)\leq\depth(I_P,M_P)$.

Sei $I=(x_1,\ldots,x_n)$ ein beliebiges Ideal von~$R$ und setze $r\define\depth(I,M)$.
Nach \ref{eis:17.4} gilt:
$H^j(M \otimes K(x_1, \ldots,x_n)) = 0$ für $j<r$ und $\neq 0$ für~$j=r$.
Die Primideale~$P$, die~$I$ enthalten und für die $\depth(I_P,M_P)=\depth(I,M)$ gilt, sind genau die Primideale, die im Träger von $H^r(M \otimes K(x_1,\ldots,x_n))$ enthalten sind.
Insbesondere gibt es auch maximale Ideale~$P$ mit dieser Eigenschaft.
Daraus folgt auch die letzte Aussage des Lemmas.
\qed}

Das folgende Lemma ist in der Theorie der Tiefe ähnlich wichtig wie der Hauptidealsatz \ref{eis:10.2} in der Theorie der Kodimension.

%18.3
\lemma[\autocite{eis} 18.3]{Seien $(R,\m)$ ein lokaler Ring, $M$ ein endlich erzeugter $R$-Modul, $I$ ein Ideal von $R$ und $y \in \m$, dann gilt:
\[\depth((I,y),M)\leq\depth(I,M)+1\]}\label{eis:18.3}

\proof{Seien $x_1,\ldots,x_n$ ein Erzeugendensystem von~$I$ und setze $r\define\depth((I,y),M)$.
Nach \ref{eis:17.4} gilt:
$H^i(M \otimes K(x_1,\ldots,x_n,y))=0$ für~$i<r$.
Nach \autocite[Korollar 17.11]{eis} und \ref{eis:4.8} folgt:
$H^i(M \otimes K(x_1,\ldots,x_n))=0$ für~$i<r-1$.
Damit gilt nach \ref{eis:17.4} $\depth(I,M)\geq r-1$.
\qed}

In der folgenden Proposition wird bewiesen, dass die Tiefe eines Ideals immer kleiner gleich der Kodimension eines Ideals ist.
Diese Aussage wird im Folgenden häufig benutzt werden.

%18.2
\prop[\autocite{eis} 18.2]{Seien~$R$ ein Ring und~$M$ ein endlich erzeugter $R$-Modul.
Sei~$I$ ein Ideal von~$R$, das~$\ann(R)$ enthält.
Sei~$l$ die Länge einer beliebigen maximalen absteigenden Primidealkette, die bei einem Primideal, das~$I$ enthält, startet und bei einem assoziierten Primideal von~$M$ endet.
Dann gilt:
$\depth(I,M) \leq l$.
Insbesondere gilt für $M=R$:
\[\depth(I)\leq\codim(I)\]}\label{eis:18.2}

\proof{Sei $A_l$ die Aussage, dass für ein $l\in\NNN$ gilt:
Falls $l$ die Länge einer maximalen absteigenden Primidealkette ist, die bei einem Primideal, das~$I$ enthält, startet und bei einem assoziierten Primideal von~$M$ endet, dann gilt:
$\depth(I,M)\leq l$.
Wir beweisen die Aussage durch Induktion über~$l$.

Sei $Q \supsetneq Q_1 \supsetneq \cdots \supsetneq Q_l$ eine maximale absteigende Primidealkette von einem Primideal~$Q$, das~$I$ enthält, zu einem assoziierten Primideal~$Q_l$ von~$M$.
Im Fall $l=0$ ist~$Q$ schon ein zu~$M$ assoziiertes Primideal.
Dann ist $\depth(Q,M)=0$ nach \ref{bsp:tiefe1}.
Da $I \subseteq Q$, ist $0\leq\depth(I,M) \leq \depth(Q,M)=0$.
Folglich ist $\depth(I,M)=0$.

Sei nun $l\geq 1$ und gelte $A_{l-1}$.
Da $\ann(M) \subseteq I \subseteq Q$, ist nach \ref{eis:2.7} $Q \in \supp(M)$ und damit gilt nach \ref{eis:18.1}:
\[\depth(I,M) \leq \depth(Q,M) \leq \depth(Q_Q,M_Q)\]
Es reicht also zu zeigen, dass $\depth(Q_Q,M_Q)\leq l$.
Sei deswegen oBdA $(R,Q)$ ein lokaler Ring, dadurch wird die Länge $l$ der Primidealkette nicht beeinflusst.

Sei~$x\in Q$ mit~$x \notin Q_{1}$.
Wir wollen zeigen, dass $\depth(Q,M) = \depth(Q_{1}+(x),M)$.
Da~$Q$ das einzige maximale Primideal von $R$ ist und die Primidealkette $Q \supsetneq Q_1 \supsetneq \cdots \supsetneq Q_l$ maximal ist, ist $Q$ das einzige Primideal, das minimal über $Q_{1}+(x)$ ist.
Daher gilt nach \autocite[Korollar 2.12]{eis}, dass~$Q$ modulo~$Q_{1}+(x)$ nilpotent ist.
Damit gilt nach \ref{bsp:tiefe1}:
$\depth(Q,M) = \depth(Q_{1}+(x),M)$.
Nach \ref{eis:18.3} gilt $\depth(Q_{1}+(x),M) \leq \depth(Q_{1},M)+1$.
Nach der Induktionsvoraussetzung gilt $\depth(Q_{1},M_Q) \leq l-1$.

Zusammenfassend gilt:
\[\depth(I,M) \leq \depth(Q,M) = \depth(Q_{1}+(x),M) \leq \depth(Q_{1},M)+1 \leq l\]

Da nach Definition \ref{lem:kodimKette} die Kodimension eines Primideals das Supremum über die Länge aller absteigenden Primidealketten ist, die bei $I$ anfangen, bzw. für beliebige Ideale das Minimum der Kodimensionen der Primideale ist, die $I$ enthalten, gilt für $M=R$:
\[\depth(I) \leq \codim(I)\]
\qed}

\bsp{Sei $x_1,\ldots,x_n$ eine $R$-Folge, dann ist $\depth(x_1,\ldots,x_n)\geq n$.
Nach \ref{eis:18.2} ist $\depth(x_1,\ldots,x_n)\leq\codim(x_1,\ldots,x_n)$ und nach \ref{eis:10.2} ist $\codim(x_1,\ldots,x_n)\leq n$.
Also ist $\depth(x_1,\ldots,x_n) = n$, wie man es auch erwartet hätte.
}\label{bsp:tiefe.Folge}

%cmrs 1.2.10d
\lemma[\autocite{cmrs} 1.2.10d]{Seien $R$ ein Ring, $I\subseteq R$ ein Ideal, $M$ ein endlich erzeugter $R$-Modul und $x_1,\ldots,x_n$ eine $M$-Folge.
Dann gilt:
\[\depth(I/(x_1,\ldots,x_n),M/(x_1,\ldots,x_n)) = \depth(I,M/(x_1,\ldots,x_n)) = \depth(I,M)-n\]
}\label{cmrs:1.2.10d}

\proof{Es gilt
\[I/(x_1,\ldots,x_n)\cdot M/(x_1,\ldots,x_n)=M/(x_1,\ldots,x_n)\]
genau dann, wenn $I\cdot M/(x_1,\ldots,x_n)=M/(x_1,\ldots,x_n)$ und genau dann, wenn $I\cdot M=M$.
Damit ist der Fall Tiefe gleich $\infty$ gezeigt.

Seien $y_1,\ldots,y_m \in I$ und $\overline{y_1},\ldots,\overline{y_m} \in I/(x_1,\ldots,x_n)$ ihre Bilder unter der Projektionsabbildung.
Es ist $y_1,\ldots,y_m$ genau dann eine $M/(x_1,\ldots,x_n)$-Folge, wenn $\overline{y_1},\ldots,\overline{y_m}$ eine $M/(x_1,\ldots,x_n)$-Folge ist.
Damit ist die erste Gleichheit gezeigt.

Sei nun $y_1,\ldots,y_m\in I$ eine maximale $M/(x_1,\ldots,x_n)$-Folge.
Dann ist $x_1,\ldots,x_n,y_1,\ldots,y_m$ eine maximale $M$-Folge.
Da nach \ref{eis:17.4} jede maximale $M$-Folge dieselbe Länge hat, gilt $\depth(I,M/(x_1,\ldots,x_n)) = \depth(I,M)-n$.
\qed}
